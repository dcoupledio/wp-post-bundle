<?php namespace Decoupled\Post\Controller;

class PostController{

    /**
     * Shows standard blog posts on the homepage
     *
     * @param      Decoupled\Core\Scope\Scope    $scope  The scope
     * @param      Decoupled\Core\Output\Output  $out    The output
     * @param      Timber\Posts            $posts  The posts
     *
     * @return     Decoupled\Core\Output\Output  ( renderable output object )
     */

    public function index( $scope, $out, $posts )
    {
        $scope['posts'] = $posts;

        return $out( '@d.post/index.html.twig', $scope );
    }

    /**
     * Shows a single Post
     *
     * @param      Decoupled\Core\Scope\Scope    $scope  The scope
     * @param      Decoupled\Core\Output\Output  $out    The output
     * @param      Timber\Post            $post    The current post
     *
     * @return     Decoupled\Core\Output\Output  ( renderable output object )
     */

    public function single( $scope, $out, $post )
    {
        if( post_password_required($post->id) ) 
            return $out('@d.post/password.html.twig', $scope );

        $scope['post'] = $post;

        return $out('@d.post/single.html.twig', $scope);        
    }

    /**
     * Post Search page
     *
     * @param      Decoupled\Core\Scope\Scope    $scope  The scope
     * @param      Decoupled\Core\Output\Output  $out    The output
     * @param      Timber\Posts                  $posts  The posts
     *
     * @return     Decoupled\Core\Output\Output  ( renderable output object )
     */

    public function search( $scope, $out, $posts )
    {
        $scope['posts'] = $posts;

        $scope['query'] = get_search_query();

        if( empty($posts) )
        {
            $scope['page_header'] = 'No Results for '.$scope['query'];    
        }
        else
        {
            $scope['page_header'] = 'Results for '.$scope['query'];
        }

        return $out('@d.post/search.html.twig', $scope );        
    }

    /**
     * Author post archive 
     *
     * @param      Decoupled\Core\Scope\Scope    $scope    The scope
     * @param      Decoupled\Core\Output\Output  $out      The output
     * @param      Timber\User                   $author   The author
     * @param      {object} archive              $archive  The archive
     *
     * @return     Decoupled\Core\Output\Output  ( renderable output object )
     */

    public function authorPosts( $scope, $out, $author, $archive )
    {
        $scope['posts'] = $archive->posts;

        if( empty($archive->posts) )
        {
            $scope['page_header'] = 'Author has no posts';
        }
        else
        {
            $scope['page_header'] = 'Posts by '.$author->name();
        }

        return $out('@d.post/archive.html.twig', $scope);
    }

    /**
     * Post Archive, by category or date
     *
     * @param      Decoupled\Core\Scope\Scope    $scope    The scope
     * @param      Decoupled\Core\Output\Output  $out      The output
     * @param      {object} archive              $archive  The archive
     *
     * @return     Decoupled\Core\Output\Output  ( renderable output object )
     */

    public function archive( $scope, $out, $archive )
    {
        $scope['page_header'] = @$archive->title ?: 'Archive: '.$archive->date;

        $scope['posts'] = $archive->posts;

        return $out('@d.post/archive.html.twig', $scope);
    }
}