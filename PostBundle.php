<?php namespace Decoupled\Post;

use Decoupled\Core\Bundle\BundleInterface;

class PostBundle implements BundleInterface{

    /**
     * @return     string  The id of the bundle.
     */

    public function getName()
    {
        return 'd.post';
    }

    /**
     * @return     string  The Bundle Dir
     */

    public function getDir()
    {
        return dirname(__FILE__);
    }

}
