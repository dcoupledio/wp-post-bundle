<?php return function( Decoupled\Core\State\StateRouter $stateRouter ){

    $stateRouter('post.default')
        ->when('$app')
        ->uses(function($scope){

            $scope['baseLayout'] = '@app/layout.html.twig';
        });

    $stateRouter('post.index')
        ->when( 'home blog' )
        ->uses('Decoupled\\Post\\Controller\\PostController@index');

    $stateRouter('post.view')
        ->when( 'single-post' )
        ->uses('Decoupled\\Post\\Controller\\PostController@single');

    $stateRouter('post.search')
        ->when( 'search' )
        ->uses('Decoupled\\Post\\Controller\\PostController@search');

    $stateRouter('post.archive.author')
        ->when( 'author archive' )
        ->uses('Decoupled\\Post\\Controller\\PostController@authorPosts');   

    $stateRouter('post.archive')
        ->when( 'archive' )
        ->not( 'author' )
        ->uses('Decoupled\\Post\\Controller\\PostController@archive');   
};